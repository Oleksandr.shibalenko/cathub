﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CatHub.DAL;
using CatHub.DAL.EF;
using CatHub.DAL.Interfaces;
using CatHub.DAL.Entities;
using Moq;
using CatHub.DAL.Repositories;

namespace CatHub.Tests.DalTest
{
    [TestFixture]
    public class VideoTest
    {
        readonly ApplicationContext _db;
        private GenericRepository<VideoCatalog> videoRepository;

        public VideoTest()
        {
            _db = new ApplicationContext("DefaultConnection");
            videoRepository = new GenericRepository<VideoCatalog>(_db);
        }


        [Test]
        public void GenericRepository_VideoCatalog_FindAll_ReturnsAllValues()
        {
            Console.WriteLine("hello");
            var videos = videoRepository.GetAll();
            Assert.AreEqual(10, videos.Count());

        }

        [Test]
        public void IndexReturnsAViewResultWithAListOfUsers()
        {
            // Arrange
            Mock<IRepository<VideoCatalog>> mock = new Mock<IRepository<VideoCatalog>>();

            mock.Setup(repo => repo.GetAll()).Returns(GetTestVideos());


            Assert.AreEqual(1, mock.Object.GetAll().Count());
        }
        private List<VideoCatalog> GetTestVideos()
        {
            var users = new List<VideoCatalog>
            {
                new VideoCatalog{ PathImg = "CuteCat1Img.jpg", Path = "CuteCat1.mp4", Description= "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Funny cat", ThemeId = 1}
            };
            return users;
        }
    }
    }

