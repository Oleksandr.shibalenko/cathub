﻿using CatHub.DAL.Entities;
using CatHub.DAL.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CatHub.Tests.DAL_Test
{
    public class TestRepository
    {

        public TestRepository()
        {
            IList<VideoCatalog> videoCatalogs = new List<VideoCatalog>
            {
                new VideoCatalog{id=1, PathImg = "CuteCat1Img.jpg", Path = "CuteCat1.mp4", Description= "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Funny cat1", ThemeId = 1},
                new VideoCatalog{id=2, PathImg = "CuteCat1Img.jpg", Path = "CuteCat1.mp4", Description= "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Funny cat2", ThemeId = 2},
                new VideoCatalog{id=3, PathImg = "CuteCat1Img.jpg", Path = "CuteCat1.mp4", Description= "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Funny cat3", ThemeId = 3}
            };
            Mock<IRepository<VideoCatalog>> mockRepo = new Mock<IRepository<VideoCatalog>>();

            mockRepo.Setup(m => m.GetAll()).Returns(videoCatalogs);
            mockRepo.Setup(m => m.Get(It.IsAny<int>())).Returns((int i) => videoCatalogs.Where(x => x.id == i).Single());
            mockRepo.Setup(m => m.Delete(It.IsAny<int>()));
            mockRepo.Setup(m => m.Create(It.IsAny<VideoCatalog>()));
            mockRepo.Setup(m => m.Update(It.IsAny<VideoCatalog>()));
            this.MockRepo = mockRepo.Object;
        }

        public IRepository<VideoCatalog> MockRepo { get; private set; }
        public TestContext TestContext { get; set; }


        [Test]
        public void GetAllTestRepository()
        {
            IList<VideoCatalog> testVideocatalogs = this.MockRepo.GetAll().ToList();
            Assert.IsNotNull(testVideocatalogs);
            Assert.AreEqual(3, testVideocatalogs.Count);

        }

        [Test]
        public void GetTestRepository()
        {
            VideoCatalog testvideoCatalog = this.MockRepo.Get(1);
            Assert.IsNotNull(testvideoCatalog);
            Assert.IsInstanceOf(typeof(VideoCatalog), testvideoCatalog);
            Assert.AreEqual("Funny cat1", testvideoCatalog.Name);
        }


        [Test]
        public void IndexReturnsAViewResultWithAListOfUsers()
        {
            // Arrange
            Mock<IRepository<VideoCatalog>> mock = new Mock<IRepository<VideoCatalog>>();

            mock.Setup(repo => repo.GetAll()).Returns(GetTestVideos());


            Assert.AreEqual(1, mock.Object.GetAll().Count());
        }
        private List<VideoCatalog> GetTestVideos()
        {
            var users = new List<VideoCatalog>
            {
                new VideoCatalog{ PathImg = "CuteCat1Img.jpg", Path = "CuteCat1.mp4", Description= "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Funny cat", ThemeId = 1}
            };
            return users;
        }
    }
}
