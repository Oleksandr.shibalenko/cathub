﻿using CatHub.BLL.Interfaces;
using CatHub.BLL.Services;
using Ninject.Modules;

namespace CatHub.Util
{
    public class ProductModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IProductService>().To<ProductService>();
        }
    }
}