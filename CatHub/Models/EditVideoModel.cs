﻿using System.ComponentModel.DataAnnotations;

namespace CatHub.Models
{
    public class EditVideoModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Theme { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }
    }
}