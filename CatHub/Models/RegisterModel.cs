﻿using System.ComponentModel.DataAnnotations;

namespace CatHub.Models
{
    public class RegisterModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Address { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }
    }
}