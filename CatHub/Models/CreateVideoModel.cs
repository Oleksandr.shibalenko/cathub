﻿using System.ComponentModel.DataAnnotations;
using System.Web;

namespace CatHub.Models
{
    public class CreateVideoModel
    {
        [Required]
        
        public HttpPostedFileBase File { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Theme { get; set; }
        [Required]
        public HttpPostedFileBase ImgFile { get; set; }
        [Required]
        [DataType(DataType.Text)]
        public string Description { get; set; }
    }
}

/*public int id { get; set; }
public string Name { get; set; }
public string Path { get; set; }
public string Date { get; set; }
public string ClientId { get; set; }
public string PathImg { get; set; }
public int ThemeId { get; set; }

public string Description { get; set; }*/