﻿using System.ComponentModel.DataAnnotations;

namespace CatHub.Models
{
    public class SearchModel
    {
        [Required]
        string Value { get; set; }
    }
}