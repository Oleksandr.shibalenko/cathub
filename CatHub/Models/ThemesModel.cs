﻿namespace CatHub.Models
{
    public class ThemesModel
    {
        public int id { get; set; }

        public string Name { get; set; }

        public string Date { get; set; }
    }
}