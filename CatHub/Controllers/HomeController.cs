﻿using AutoMapper;
using CatHub.BLL.DTO;
using CatHub.BLL.Interfaces;
using CatHub.Models;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CatHub.Controllers
{
    public class HomeController : Controller
    {

        IProductService productService;
        public HomeController(IProductService serv)
        {
            productService = serv;
        }
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }
        public ActionResult Themes()
        {
            IEnumerable<ThemesDTO> ThemesDtos = productService.GetThemes();
            var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<ThemesDTO, ThemesModel>()).CreateMapper();
            var theme = mapperThemes.Map<IEnumerable<ThemesDTO>, List<ThemesModel>>(ThemesDtos);
            ViewBag.Themess = theme;
            return PartialView();
        }



        public ActionResult Index(int? page)
        {


            IEnumerable<VideoCatalogDTO> videoCatalogDtos = productService.GetVideoCatalogs();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalogDTO, VideoCatalogModel>()).CreateMapper();
            var video = mapper.Map<IEnumerable<VideoCatalogDTO>, List<VideoCatalogModel>>(videoCatalogDtos); //.Where(th=> th.ThemeId == );
            int pageSize = 12;
            int pageNumber = (page ?? 1);


            return View(video.ToPagedList(pageNumber, pageSize));

        }

        public ActionResult AdminMenu()
        {

            return View();
        }

        public ActionResult Theme(int? id)
        {


            IEnumerable<VideoCatalogDTO> videoCatalogDtos = productService.GetVideoCatalogs();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalogDTO, VideoCatalogModel>()).CreateMapper();
            var video = mapper.Map<IEnumerable<VideoCatalogDTO>, List<VideoCatalogModel>>(videoCatalogDtos).Where(th => th.ThemeId == id);



            return View("Index", video.ToPagedList(1, 50));

        }

        public ActionResult VideoByUser(string id)
        {


            IEnumerable<VideoCatalogDTO> videoCatalogDtos = productService.GetVideoCatalogs();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalogDTO, VideoCatalogModel>()).CreateMapper();
            var video = mapper.Map<IEnumerable<VideoCatalogDTO>, List<VideoCatalogModel>>(videoCatalogDtos).Where(th => th.ClientId == id);



            return View("Index", video.ToPagedList(1, 50));

        }

        [HttpPost]
        public ActionResult Search(string id)
        {
            if (id == null)
            {
                id = "";
            }

            IEnumerable<VideoCatalogDTO> videoCatalogDtos = productService.GetVideoCatalogs();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalogDTO, VideoCatalogModel>()).CreateMapper();
            var video = mapper.Map<IEnumerable<VideoCatalogDTO>, List<VideoCatalogModel>>(videoCatalogDtos).Where(th => th.Name.Contains(id));



            return View("Index", video.ToPagedList(1, 50));

        }
        public ActionResult ConcreteVideo(int? id)
        {

            try
            {
                VideoCatalogDTO videoCatalogDtos = productService.GetVideoCatalog(id);
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalogDTO, VideoCatalogModel>()).CreateMapper();
                var video = mapper.Map<VideoCatalogDTO, VideoCatalogModel>(videoCatalogDtos);
                ViewBag.Autor = UserService.GetUserById(video.ClientId).Name;

                //во вью добавляем модель
                return View(video);
            }
            catch 
            {
                return View("Error");
            }
            


        }

    }
}