﻿using AutoMapper;
using CatHub.BLL.DTO;
using CatHub.BLL.Interfaces;
using CatHub.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CatHub.Controllers
{
    public class VideoController : Controller
    {
        // GET: Video
        IProductService productService;
        public VideoController(IProductService serv)
        {
            productService = serv;
        }

        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        public ActionResult Index()
        {

            return View();
        }

        [Authorize]
        public ActionResult CreateVideo()
        {
            IEnumerable<ThemesDTO> ThemesDtos = productService.GetThemes();
            var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<ThemesDTO, ThemesModel>()).CreateMapper();
            var theme = mapperThemes.Map<IEnumerable<ThemesDTO>, List<ThemesModel>>(ThemesDtos);

            List<string> list = new List<string>();
            foreach (var d in theme)
            {
                list.Add(d.Name);
            }

            SelectList selectLists = new SelectList(list);
            ViewBag.List = selectLists;

            return View();
        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult CreateVideo(CreateVideoModel video)
        {

            try
            {
                if (ModelState.IsValid && video.File.ContentLength > 0 && video.ImgFile.ContentLength > 0 && video.File.ContentLength < 5242880 && video.ImgFile.ContentLength < 5242880)
                {
                    string _FileName = Path.GetFileName(video.File.FileName);
                    string _path = Path.Combine(Server.MapPath("~/VideoFiles"), _FileName);
                    video.File.SaveAs(_path);

                    string _ImgName = Path.GetFileName(video.ImgFile.FileName);
                    string _ImgPath = Path.Combine(Server.MapPath("~/ImgFiles"), _ImgName);
                    video.ImgFile.SaveAs(_ImgPath);
                    int idTheme = productService.GetThemes().Where(id => id.Name == video.Theme).First().id;

                    productService.CreateVideoCatalog(new VideoCatalogDTO
                    {
                        ClientId = UserService.GetUser(this.User.Identity.Name).Id,
                        Date = DateTime.UtcNow.ToString(),
                        Description = video.Description,
                        Name = video.Name,
                        ThemeId = idTheme,
                        Path = video.File.FileName,
                        PathImg = video.ImgFile.FileName
                    });

                    return RedirectToAction("MyVideo", "Video");
                }
                else
                {
                    IEnumerable<ThemesDTO> ThemesDtos = productService.GetThemes();
                    var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<ThemesDTO, ThemesModel>()).CreateMapper();
                    var theme = mapperThemes.Map<IEnumerable<ThemesDTO>, List<ThemesModel>>(ThemesDtos);

                    List<string> list = new List<string>();
                    foreach (var d in theme)
                    {
                        list.Add(d.Name);
                    }

                    SelectList selectLists = new SelectList(list);
                    ViewBag.List = selectLists;
                    return View(video);
                }
            }
            catch
            {
                ViewBag.Message = "File upload failed!!";
                return RedirectToAction("MyVideo", "Video");
            }






        }

        [Authorize]
        public ActionResult EditVideo(int? id)
        {

            VideoCatalogDTO videoCatalogDTO = productService.GetVideoCatalog(id);

            if (videoCatalogDTO.ClientId == UserService.GetUser(this.User.Identity.Name).Id || this.User.IsInRole("admin"))
            {
                IEnumerable<ThemesDTO> ThemesDtos = productService.GetThemes();
                var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<ThemesDTO, ThemesModel>()).CreateMapper();
                var theme = mapperThemes.Map<IEnumerable<ThemesDTO>, List<ThemesModel>>(ThemesDtos);

                List<string> list = new List<string>();
                foreach (var d in theme)
                {
                    list.Add(d.Name);
                }

                SelectList selectLists = new SelectList(list);
                ViewBag.List = selectLists;



                EditVideoModel editVideo = new EditVideoModel
                {
                    Description = videoCatalogDTO.Description,
                    Name = videoCatalogDTO.Name,
                    Id = videoCatalogDTO.id,
                    Theme = productService.GetThemes().Where(d => d.id == videoCatalogDTO.ThemeId).First().Name
                };


                return View(editVideo);
            }
            else
            {
                return RedirectToAction("MyVideo");
            }

        }

        [Authorize]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult EditVideo(EditVideoModel video)
        {
            if (ModelState.IsValid)
            {
                if (productService.GetVideoCatalog(video.Id).ClientId == UserService.GetUser(this.User.Identity.Name).Id || this.User.IsInRole("admin"))
                {
                    VideoCatalogDTO videoDto = productService.GetVideoCatalog(video.Id);

                    videoDto.Name = video.Name;
                    videoDto.ThemeId = productService.GetThemes().Where(id => id.Name == video.Theme).First().id;
                    videoDto.Description = video.Description;
                    productService.UpdateVideoCatalog(videoDto);
                    return RedirectToAction("ConcreteVideo", "Home", new { id = video.Id });
                }
                else
                {
                    return RedirectToAction("MyVideo", "Video");
                }
            }
            else
            {
                IEnumerable<ThemesDTO> ThemesDtos = productService.GetThemes();
                var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<ThemesDTO, ThemesModel>()).CreateMapper();
                var theme = mapperThemes.Map<IEnumerable<ThemesDTO>, List<ThemesModel>>(ThemesDtos);

                List<string> list = new List<string>();
                foreach (var d in theme)
                {
                    list.Add(d.Name);
                }

                SelectList selectLists = new SelectList(list);
                ViewBag.List = selectLists;
                return View(video);
            }


        }

        [Authorize]
        public ActionResult MyVideo()
        {
            IEnumerable<VideoCatalogDTO> videoCatalogDtos = productService.GetVideoCatalogs();
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalogDTO, VideoCatalogModel>()).CreateMapper();

            var video = mapper.Map<IEnumerable<VideoCatalogDTO>, List<VideoCatalogModel>>(videoCatalogDtos).Where(th => th.ClientId == UserService.GetUser(this.User.Identity.Name).Id);

            if (this.User.IsInRole("admin"))
            {
                video = mapper.Map<IEnumerable<VideoCatalogDTO>, List<VideoCatalogModel>>(videoCatalogDtos);
            }

            return View(video);
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            if (productService.GetVideoCatalog(id).ClientId == UserService.GetUser(this.User.Identity.Name).Id || this.User.IsInRole("admin"))
            {
                productService.Delete(id);
            }

            return RedirectToAction("MyVideo", "Video");

        }


    }
}