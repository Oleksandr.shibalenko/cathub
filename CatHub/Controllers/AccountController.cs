﻿using AutoMapper;
using CatHub.BLL.DTO;
using CatHub.BLL.Infrastructure;
using CatHub.BLL.Interfaces;
using CatHub.Models;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CatHub.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }



        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Login(string returnUrl)
        {

            // UserService.Edit("asdasd@fas", "aasdasd228@228");
            ViewBag.returnUrl = returnUrl;
            return View();
        }

        [Authorize]
        [HttpGet] // [Authorize]
        public ActionResult Edit()
        {
            UserDTO userDto = UserService.GetUser(this.User.Identity.Name);

            return View("Edit", new RegisterModel { Address = userDto.Address, Email = userDto.Email, Name = userDto.Name });
        }
        [Authorize]
        [HttpPost]
        public async Task<ActionResult> Edit(RegisterModel model)
        {


            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Address = model.Address,
                    Name = model.Name,
                    Role = "user",
                    Date = DateTime.UtcNow.ToString()
                };
                OperationDetails operationDetails = await UserService.Edit(userDto, this.User.Identity.Name);
                if (operationDetails.Succedeed)
                    return RedirectToAction("Index", "Home");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);

            // UserService.Edit(User.Identity.Name, Address);

        }


        [Authorize(Roles = "admin")]
        [HttpGet] // [Authorize]
        public ActionResult EditUserByAdmin(string id)
        {
            IEnumerable<UserDTO> userFindRole = UserService.GetUsers();
          
            UserDTO userDto = UserService.GetUserById(id);
           
            var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, EditUsersByAdmin>()).CreateMapper();
            var theme = mapperThemes.Map<IEnumerable<UserDTO>, List<EditUsersByAdmin>>(userFindRole);



            List<string> list = new List<string>();

            list.Add("admin");
            list.Add("user");

            SelectList selectLists = new SelectList(list);
          
            ViewBag.List = selectLists;
            return View("EditUserByAdmin", new EditUsersByAdmin { Address = userDto.Address, Email = userDto.Email, Name = userDto.Name , Role = userDto.Role });
        }


        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult> EditUserByAdmin(EditUsersByAdmin model)
        {


            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Address = model.Address,
                    Name = model.Name,
                    Role = model.Role,
                    Date = DateTime.UtcNow.ToString()
                };
                OperationDetails operationDetails = await UserService.Edit(userDto, model.Email);
                if (operationDetails.Succedeed)
                    return RedirectToAction("EditUsers", "Account");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);


        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO { Email = model.Email, Password = model.Password };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    if (String.IsNullOrEmpty(returnUrl))
                        return RedirectToAction("Index", "Home");
                    return Redirect(returnUrl);
                }
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {


            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Address = model.Address,
                    Name = model.Name,
                    Role = "user",
                    Date = DateTime.UtcNow.ToString()
                };
                OperationDetails operationDetails = await UserService.Create(userDto);
                if (operationDetails.Succedeed)
                    return RedirectToAction("Login", "Account");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);
        }

        [Authorize(Roles = "admin")]
        public async Task<ActionResult> EditUsers()
        {

            IEnumerable<UserDTO> userDTOs = UserService.GetUsers();
            var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<UserDTO, AdminUsers>()).CreateMapper();
            var users = mapperThemes.Map<IEnumerable<UserDTO>, List<AdminUsers>>(userDTOs);

            return View(users);


        }
        [Authorize(Roles = "admin")]
        public ActionResult DeleteUser(string id)
        {

            UserService.Delete(id);

            return RedirectToAction("EditUsers", "Account");
        }





    }
}