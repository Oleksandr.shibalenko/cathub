﻿using AutoMapper;
using CatHub.BLL.DTO;
using CatHub.BLL.Interfaces;
using CatHub.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CatHub.Controllers
{
    public class PartialController : Controller
    {
        IProductService productService;
        public PartialController(IProductService serv)
        {
            productService = serv;
        }
        // GET: Partial
        public ActionResult Themes()
        {
            IEnumerable<ThemesDTO> ThemesDtos = productService.GetThemes();
            var mapperThemes = new MapperConfiguration(cfg => cfg.CreateMap<ThemesDTO, ThemesModel>()).CreateMapper();
            var theme = mapperThemes.Map<IEnumerable<ThemesDTO>, List<ThemesModel>>(ThemesDtos);
            ViewBag.Themes = theme;
            return PartialView("Partial");
        }



    }
}