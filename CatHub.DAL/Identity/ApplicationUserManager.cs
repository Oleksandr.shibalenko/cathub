﻿using CatHub.DAL.Entities;
using Microsoft.AspNet.Identity;

namespace CatHub.DAL.Identity
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
                : base(store)
        {
        }
    }
}