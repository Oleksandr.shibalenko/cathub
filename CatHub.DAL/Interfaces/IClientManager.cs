﻿using CatHub.DAL.Entities;
using System;
using System.Collections.Generic;

namespace CatHub.DAL.Interfaces
{
    public interface IClientManager : IDisposable
    {
        void Create(ClientProfile item);
        void Delete(ClientProfile item);
        IEnumerable<ClientProfile> GetAllUsers();
    }
}