﻿using CatHub.DAL.Entities;
using CatHub.DAL.Identity;
using System;
using System.Threading.Tasks;

namespace CatHub.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {


        IRepository<VideoCatalog> VideoCatalogs { get; }
        IRepository<Themes> Themes { get; }
        ApplicationUserManager UserManager { get; }
        IClientManager ClientManager { get; }
        ApplicationRoleManager RoleManager { get; }
        Task SaveAsync();
        void Save();
    }
}