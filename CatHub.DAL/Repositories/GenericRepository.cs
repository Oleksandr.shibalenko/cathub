﻿using CatHub.DAL.EF;
using CatHub.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace CatHub.DAL.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        private ApplicationContext _context;
        private readonly DbSet<TEntity> _dbSet;
        public GenericRepository(ApplicationContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public TEntity Get(int id)
        {
            return _dbSet.Find(id);
        }

        public void Create(TEntity item)
        {
            _dbSet.Add(item);
        }

        public void Update(TEntity video)
        {
            _context.Entry(video).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public IEnumerable<TEntity> Find(Func<TEntity, Boolean> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public void Delete(int id)
        {
            TEntity item = _dbSet.Find(id);
            if (item != null)
                _dbSet.Remove(item);
        }

    }
}
