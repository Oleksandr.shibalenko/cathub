﻿using CatHub.DAL.EF;
using CatHub.DAL.Entities;
using CatHub.DAL.Identity;
using CatHub.DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;

namespace CatHub.DAL.Repositories
{
    public class IdentityUnitOfWork : IUnitOfWork
    {
        private ApplicationContext db;

        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;
        private GenericRepository<VideoCatalog> videoRepository;
        private GenericRepository<Themes> themeRepository;
        private IClientManager clientManager;

        public IdentityUnitOfWork(string connectionString)
        {
            db = new ApplicationContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(db));
            clientManager = new ClientManager(db);
            videoRepository = new GenericRepository<VideoCatalog>(db);
            themeRepository = new GenericRepository<Themes>(db);
        }

        public IRepository<VideoCatalog> VideoCatalogs
        {
            get
            {
                if (videoRepository == null)
                    videoRepository = new GenericRepository<VideoCatalog>(db);
                return videoRepository;
            }
        }
        public IRepository<Themes> Themes
        {
            get
            {
                if (themeRepository == null)
                    themeRepository = new GenericRepository<Themes>(db);
                return themeRepository;
            }
        }

        public ApplicationUserManager UserManager
        {
            get { return userManager; }
        }

        public IClientManager ClientManager
        {
            get { return clientManager; }
        }

        public ApplicationRoleManager RoleManager
        {
            get { return roleManager; }
        }

        public async Task SaveAsync()
        {
            await db.SaveChangesAsync();
        }

        public void Save()
        {
            db.SaveChanges();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    clientManager.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}