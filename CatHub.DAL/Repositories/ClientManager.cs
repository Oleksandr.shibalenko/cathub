﻿using CatHub.DAL.EF;
using CatHub.DAL.Entities;
using CatHub.DAL.Interfaces;
using System.Collections.Generic;

namespace CatHub.DAL.Repositories
{
    public class ClientManager : IClientManager
    {
        public ApplicationContext Database { get; set; }
        public ClientManager(ApplicationContext db)
        {
            Database = db;
        }

        public void Create(ClientProfile item)
        {
            Database.ClientProfiles.Add(item);
            Database.SaveChanges();
        }
        public void Delete(ClientProfile item)
        {
            Database.ClientProfiles.Remove(item);
            Database.SaveChanges();
        }
        public IEnumerable<ClientProfile> GetAllUsers()
        {
            return Database.ClientProfiles;
        }
        public void Dispose()
        {
            Database.Dispose();
        }
    }
}