﻿namespace CatHub.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class test25 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VideoCatalogs", "ClientCreator_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.VideoCatalogs", "ClientCreator_Id");
            AddForeignKey("dbo.VideoCatalogs", "ClientCreator_Id", "dbo.ClientProfiles", "Id");
            DropColumn("dbo.VideoCatalogs", "ClientCreator");
        }

        public override void Down()
        {
            AddColumn("dbo.VideoCatalogs", "ClientCreator", c => c.String());
            DropForeignKey("dbo.VideoCatalogs", "ClientCreator_Id", "dbo.ClientProfiles");
            DropIndex("dbo.VideoCatalogs", new[] { "ClientCreator_Id" });
            DropColumn("dbo.VideoCatalogs", "ClientCreator_Id");
        }
    }
}
