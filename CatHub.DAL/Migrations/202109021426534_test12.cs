﻿namespace CatHub.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class test12 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ClientProfiles", "Date", c => c.String());
            AddColumn("dbo.Themes", "Date", c => c.String());
            AddColumn("dbo.VideoCatalogs", "Date", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.VideoCatalogs", "Date");
            DropColumn("dbo.Themes", "Date");
            DropColumn("dbo.ClientProfiles", "Date");
        }
    }
}
