﻿namespace CatHub.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class test1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.VideoCatalogs", "Theme_id", "dbo.Themes");
            DropIndex("dbo.VideoCatalogs", new[] { "Theme_id" });
            RenameColumn(table: "dbo.VideoCatalogs", name: "Theme_id", newName: "ThemeId");
            AddColumn("dbo.VideoCatalogs", "ClientId", c => c.String());
            AddColumn("dbo.Themes", "ClientProfile_Id", c => c.String(maxLength: 128));
            AlterColumn("dbo.VideoCatalogs", "ThemeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Themes", "ClientProfile_Id");
            CreateIndex("dbo.VideoCatalogs", "ThemeId");
            AddForeignKey("dbo.Themes", "ClientProfile_Id", "dbo.ClientProfiles", "Id");
            AddForeignKey("dbo.VideoCatalogs", "ThemeId", "dbo.Themes", "id", cascadeDelete: true);
        }

        public override void Down()
        {
            DropForeignKey("dbo.VideoCatalogs", "ThemeId", "dbo.Themes");
            DropForeignKey("dbo.Themes", "ClientProfile_Id", "dbo.ClientProfiles");
            DropIndex("dbo.VideoCatalogs", new[] { "ThemeId" });
            DropIndex("dbo.Themes", new[] { "ClientProfile_Id" });
            AlterColumn("dbo.VideoCatalogs", "ThemeId", c => c.Int());
            DropColumn("dbo.Themes", "ClientProfile_Id");
            DropColumn("dbo.VideoCatalogs", "ClientId");
            RenameColumn(table: "dbo.VideoCatalogs", name: "ThemeId", newName: "Theme_id");
            CreateIndex("dbo.VideoCatalogs", "Theme_id");
            AddForeignKey("dbo.VideoCatalogs", "Theme_id", "dbo.Themes", "id");
        }
    }
}
