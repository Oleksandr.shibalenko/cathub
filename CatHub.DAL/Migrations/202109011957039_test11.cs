﻿namespace CatHub.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class test11 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Themes", "VideoCatalogId");
        }

        public override void Down()
        {
            AddColumn("dbo.Themes", "VideoCatalogId", c => c.Int(nullable: false));
        }
    }
}
