﻿namespace CatHub.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class test13 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VideoCatalogs", "Description", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.VideoCatalogs", "Description");
        }
    }
}
