﻿namespace CatHub.DAL.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class test122 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.VideoCatalogs", "PathImg", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.VideoCatalogs", "PathImg");
        }
    }
}
