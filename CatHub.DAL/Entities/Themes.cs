﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CatHub.DAL.Entities
{
    public class Themes
    {
        [Key]
        public int id { get; set; }

        public string Name { get; set; }

        public string Date { get; set; }
        public virtual ICollection<VideoCatalog> VideoCatalog { get; set; }



    }
}
