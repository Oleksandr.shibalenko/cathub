﻿using System.ComponentModel.DataAnnotations;

namespace CatHub.DAL.Entities
{
    public class VideoCatalog
    {
        [Key]
        public int id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string PathImg { get; set; }
        public string Description { get; set; }
        public string Date { get; set; }
        public string ClientId { get; set; }

        public virtual ClientProfile ClientCreator { get; set; }

        public int ThemeId { get; set; }
        // public string ClientCreator { get; set; }
        public virtual Themes Theme { get; set; }

    }
}
