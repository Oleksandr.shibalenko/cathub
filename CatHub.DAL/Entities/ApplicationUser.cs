﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace CatHub.DAL.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public virtual ClientProfile ClientProfile { get; set; }
    }
}