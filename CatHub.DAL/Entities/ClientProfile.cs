﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CatHub.DAL.Entities
{
    public class ClientProfile
    {
        [Key]
        [ForeignKey("ApplicationUser")]
        public string Id { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }

        public string Date { get; set; }
        public ICollection<Themes> Themes { get; set; }

        public ICollection<VideoCatalog> Videos { get; set; }

        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}