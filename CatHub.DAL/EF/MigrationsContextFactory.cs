﻿using System.Data.Entity.Infrastructure;

namespace CatHub.DAL.EF
{
    public class MigrationsContextFactory : IDbContextFactory<ApplicationContext>
    {
        public ApplicationContext Create()
        {
            return new ApplicationContext("DefaultConnection");
        }
    }
}