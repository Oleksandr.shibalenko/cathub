﻿using CatHub.DAL.Entities;
using CatHub.DAL.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;

namespace CatHub.DAL.EF
{
    public class ContextInitializer : DropCreateDatabaseAlways<ApplicationContext>
    {
        protected override void Seed(ApplicationContext context)
        {
            var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


            var role1 = new IdentityRole { Name = "admin" };
            var role2 = new IdentityRole { Name = "user" };
            roleManager.Create(role1);
            roleManager.Create(role2);

            var admin = new ApplicationUser { Email = "admin@google.com", UserName = "admin@google.com", ClientProfile = new ClientProfile { Date = DateTime.UtcNow.ToString(), Address = "Kharkiv", Name = "AdminName" } };
            var user = new ApplicationUser { Email = "user@google.com", UserName = "user@google.com", ClientProfile = new ClientProfile { Date = DateTime.UtcNow.ToString(), Address = "Kiev", Name = "UserName" } };
            string passwordAdmin = "godgod";
            string passwordUser = "qwerty";

            var result2 = userManager.Create(user, passwordUser);
            var result1 = userManager.Create(admin, passwordAdmin);

            if (result1.Succeeded)
            {
                userManager.AddToRole(admin.Id, role1.Name);

            }
            if (result2.Succeeded)
            {
                userManager.AddToRole(user.Id, role2.Name);

            }


            Collection<Themes> themes = new Collection<Themes>();
            themes.Add(new Themes { Name = "Cute cats", Date = DateTime.UtcNow.ToString() });
            themes.Add(new Themes { Name = "Playful cats", Date = DateTime.UtcNow.ToString() });
            context.Themes.AddRange(themes);

            Collection<VideoCatalog> videoCatalogs = new Collection<VideoCatalog>();
            videoCatalogs.Add(new VideoCatalog { PathImg = "CuteCat1Img.jpg", Path = "CuteCat1.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Funny cat", ThemeId = 1, ClientId = context.ClientProfiles.Where(id => id.Name == "AdminName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "CuteCat2Img.jpg", Path = "CuteCat2.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "So cute cat", ThemeId = 1, ClientId = context.ClientProfiles.Where(id => id.Name == "AdminName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "CuteCat3Img.jpg", Path = "CuteCat3.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Wow its cat", ThemeId = 1, ClientId = context.ClientProfiles.Where(id => id.Name == "AdminName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "CuteCat4Img.jpg", Path = "CuteCat4.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Cat cat!!", ThemeId = 1, ClientId = context.ClientProfiles.Where(id => id.Name == "AdminName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "CuteCat5Img.jpg", Path = "CuteCat5.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Cat cat wowow!!", ThemeId = 1, ClientId = context.ClientProfiles.Where(id => id.Name == "AdminName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "PlayCat1Img.jpg", Path = "PlayCat1.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Amazing cat", ThemeId = 2, ClientId = context.ClientProfiles.Where(id => id.Name == "UserName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "PlayCat2Img.jpg", Path = "PlayCat2.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "LOL Cat", ThemeId = 2, ClientId = context.ClientProfiles.Where(id => id.Name == "UserName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "PlayCat3Img.jpg", Path = "PlayCat3.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Wow Cats", ThemeId = 2, ClientId = context.ClientProfiles.Where(id => id.Name == "UserName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "PlayCat4Img.jpg", Path = "PlayCat4.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Cat!!!!!!", ThemeId = 2, ClientId = context.ClientProfiles.Where(id => id.Name == "UserName").First().Id });
            videoCatalogs.Add(new VideoCatalog { PathImg = "PlayCat5Img.jpg", Path = "PlayCat5.mp4", Description = "Video about very beautiful cats. Although cats cannot be beautiful.", Date = DateTime.UtcNow.ToString(), Name = "Cat!!!!!!Cute!!", ThemeId = 2, ClientId = context.ClientProfiles.Where(id => id.Name == "UserName").First().Id });
            context.VideoCatalogs.AddRange(videoCatalogs);


            base.Seed(context);
        }
    }
}
