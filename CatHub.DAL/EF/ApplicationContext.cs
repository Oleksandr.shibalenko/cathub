using CatHub.DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace CatHub.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext(string conectionString) : base(conectionString)
        {
          //  Database.SetInitializer(new ContextInitializer());
        }

        public DbSet<ClientProfile> ClientProfiles { get; set; }

        public DbSet<VideoCatalog> VideoCatalogs { get; set; }
        public DbSet<Themes> Themes { get; set; }


    }
}