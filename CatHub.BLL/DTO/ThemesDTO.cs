﻿namespace CatHub.BLL.DTO
{
    public class ThemesDTO
    {
        public int id { get; set; }

        public string Name { get; set; }

        public string Date { get; set; }

    }
}
