﻿namespace CatHub.BLL.DTO
{
    public class VideoCatalogDTO
    {
        public int id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Date { get; set; }
        public string ClientId { get; set; }
        public string PathImg { get; set; }
        public int ThemeId { get; set; }

        public string Description { get; set; }




    }
}
