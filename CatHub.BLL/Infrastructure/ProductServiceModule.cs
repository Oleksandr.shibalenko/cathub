﻿using CatHub.DAL.Interfaces;
using CatHub.DAL.Repositories;
using Ninject.Modules;

namespace CatHub.BLL.Infrastructure
{
    public class ProductServiceModule : NinjectModule
    {

        private string connectionString;
        public ProductServiceModule(string connection)
        {
            connectionString = connection;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<IdentityUnitOfWork>().WithConstructorArgument(connectionString);
        }

    }
}
