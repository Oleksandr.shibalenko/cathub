﻿using CatHub.BLL.DTO;
using CatHub.BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CatHub.BLL.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<OperationDetails> Create(UserDTO userDto);

        Task<OperationDetails> Edit(UserDTO userDto, string email);
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);
        Task SetInitialData(UserDTO adminDto, List<string> roles);
        UserDTO GetUser(string name);
        UserDTO GetUserById(string id);
        IEnumerable<UserDTO> GetUsers();
        void Delete(string id);
    }
}