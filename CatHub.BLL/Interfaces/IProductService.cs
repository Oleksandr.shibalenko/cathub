﻿using CatHub.BLL.DTO;
using CatHub.BLL.Infrastructure;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CatHub.BLL.Interfaces
{
    public interface IProductService
    {
        /// <summary>
        ///     Test comments
        /// </summary>
        /// <returns> <see cref="VideoCatalogDTO"/></returns>
        VideoCatalogDTO GetVideoCatalog(int? id);
        IEnumerable<VideoCatalogDTO> GetVideoCatalogs();
        Task<OperationDetails> CreateVideoCatalog(VideoCatalogDTO videoCatalogDTO);
        IEnumerable<ThemesDTO> GetThemes();
        Task<OperationDetails> UpdateVideoCatalog(VideoCatalogDTO videoDTO);
        void Delete(int Id);
        void Dispose();

    }
}
