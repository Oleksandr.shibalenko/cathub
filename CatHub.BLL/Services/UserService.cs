﻿using AutoMapper;
using CatHub.BLL.DTO;
using CatHub.BLL.Infrastructure;
using CatHub.BLL.Interfaces;
using CatHub.DAL.Entities;
using CatHub.DAL.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace CatHub.BLL.Services
{
    public class UserService : IUserService
    {
        IUnitOfWork Database { get; set; }

        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    Email = userDto.Email,
                    UserName = userDto.Email
                };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");
                // добавляем роль
                await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
                // создаем профиль клиента
                ClientProfile clientProfile = new ClientProfile
                {
                    Id = user.Id,
                    Address = userDto.Address,
                    Name = userDto.Name,
                    Date = userDto.Date
                };
                Database.ClientManager.Create(clientProfile);
                await Database.SaveAsync();

                return new OperationDetails(true, "Registration completed successfully", "");
            }
            else
            {
                return new OperationDetails(false, "User with this login already exists", "Email");
            }
        }
        public async Task<OperationDetails> Edit(UserDTO userDto, string name)
        {
            ApplicationUser user = await Database.UserManager.FindByNameAsync(name);

            if (user != null)
            {
                Database.UserManager.RemovePassword(user.Id);
                Database.UserManager.AddPassword(user.Id, userDto.Password);
                user.ClientProfile.Address = userDto.Address;
                user.ClientProfile.Name = userDto.Name;
                user.UserName = userDto.Email;
                await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);
               
                

                Database.UserManager.Update(user);
                return new OperationDetails(true, "Edited complited", "");
            }
            else
            {
                return new OperationDetails(false, "User not found", "Email");
            }



        }

      


        public UserDTO GetUser(string name)
        {
            ApplicationUser user = Database.UserManager.FindByName(name);
           
            return new UserDTO
            {
                Address = user.ClientProfile.Address,
                Date = user.ClientProfile.Date,
                Email = user.Email,
                Name = user.ClientProfile.Name,
                UserName = user.UserName,
               
                Id = user.Id
            };
        }

        public UserDTO GetUserById(string id)
        {
            ApplicationUser user = Database.UserManager.FindById(id);
            return new UserDTO
            {
                Address = user.ClientProfile.Address,
                Date = user.ClientProfile.Date,
                Email = user.Email,
                Name = user.ClientProfile.Name,
                UserName = user.UserName,
                Id = user.Id
            };
        }

        public IEnumerable<UserDTO> GetUsers()
        {
            var mapper = new MapperConfiguration(cnf => cnf.CreateMap<ClientProfile, UserDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<ClientProfile>, List<UserDTO>>(Database.ClientManager.GetAllUsers());
        }

        public void Delete(string id)
        {
            ApplicationUser user = Database.UserManager.FindById(id);
            Database.ClientManager.Delete(user.ClientProfile);
            Database.UserManager.Delete(user);
        }

        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;
            ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        public async Task SetInitialData(UserDTO adminDto, List<string> roles)
        {
            foreach (string roleName in roles)
            {
                var role = await Database.RoleManager.FindByNameAsync(roleName);
                if (role == null)
                {
                    role = new ApplicationRole { Name = roleName };
                    await Database.RoleManager.CreateAsync(role);
                }
            }
            await Create(adminDto);
        }

        public void Dispose()
        {
            Database.Dispose();
        }
    }
}