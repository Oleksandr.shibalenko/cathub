﻿using AutoMapper;
using CatHub.BLL.DTO;
using CatHub.BLL.Infrastructure;
using CatHub.BLL.Interfaces;
using CatHub.DAL.Entities;
using CatHub.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CatHub.BLL.Services
{
    public class ProductService : IProductService
    {

        IUnitOfWork Database { get; set; }

        public ProductService(IUnitOfWork uow)
        {
            Database = uow;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public VideoCatalogDTO GetVideoCatalog(int? id)
        {
            if (id == null)
                throw new ValidationException("id not found", "");
            var video = Database.VideoCatalogs.Get(id.Value);
            if (video == null)
                throw new ValidationException("Video not found", "");

            return new VideoCatalogDTO
            {
                Name = video.Name,
                ThemeId = video.ThemeId,
                id = video.id,
                Path = video.Path,
                ClientId = video.ClientId,
                Date = video.Date,
                Description = video.Description,
                PathImg = video.PathImg
            };
        }

        public IEnumerable<VideoCatalogDTO> GetVideoCatalogs()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<VideoCatalog, VideoCatalogDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<VideoCatalog>, List<VideoCatalogDTO>>(Database.VideoCatalogs.GetAll());

        }

        public IEnumerable<ThemesDTO> GetThemes()
        {

            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Themes, ThemesDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<Themes>, List<ThemesDTO>>(Database.Themes.GetAll());

        }

        public async Task<OperationDetails> CreateVideoCatalog(VideoCatalogDTO videoDTO)
        {


            VideoCatalog video = new VideoCatalog
            {

                ClientId = videoDTO.ClientId,
                id = videoDTO.id,
                Name = videoDTO.Name,
                ThemeId = videoDTO.ThemeId,
                Path = videoDTO.Path,
                Date = videoDTO.Date,
                PathImg = videoDTO.PathImg,
                Description = videoDTO.Description

            };
            Database.VideoCatalogs.Create(video);
            await Database.SaveAsync();

            return new OperationDetails(true, "Create complited", "");
        }

        public async Task<OperationDetails> UpdateVideoCatalog(VideoCatalogDTO videoDTO)
        {

            VideoCatalog videoCatalog = Database.VideoCatalogs.Get(videoDTO.id);
            videoCatalog.Name = videoDTO.Name;
            videoCatalog.ThemeId = videoDTO.ThemeId;
            videoCatalog.Description = videoDTO.Description;

            Database.VideoCatalogs.Update(videoCatalog);

            await Database.SaveAsync();
            return new OperationDetails(true, "Edit complited", "");
        }


        public void Delete(int Id)
        {
            Database.VideoCatalogs.Delete(Id);
            Database.SaveAsync();

        }

        public void Dispose()
        {
            Database.Dispose();
        }


    }
}
