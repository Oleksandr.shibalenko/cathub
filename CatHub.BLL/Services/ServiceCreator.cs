﻿using CatHub.BLL.Interfaces;
using CatHub.DAL.Repositories;

namespace CatHub.BLL.Services
{
    public class ServiceCreator : IServiceCreator
    {
        public IUserService CreateUserService(string connection)
        {
            return new UserService(new IdentityUnitOfWork(connection));
        }
    }
}